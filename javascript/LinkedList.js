class Node {
    constructor(data) {
        this.data = data;
        this.data = null;
    }
}

class LinkedList {
    constructor(head = null) {
        this.head = head;
    }
}

var node1 = new Node(1);
var node2 = new Node(2);
var node3 = new Node(3);

node1.next = node2;
node2.next = node3;

var linkedlist = new LinkedList(node1);

function traverse(head) {
    var val = head;
    while (val != null) {
        console.log(val.data);
        val = val.next;
    }
}

function insert(head, value) {
    var newNode = new Node(value);
    if (head == null || value < head.data) {
        newNode.next = head;
        return newNode;
    }
    previousNode = head;
    currentNode = head.next;

    while (currentNode != null && currentNode.data < value) {
        previousNode = currentNode;
        currentNode = currentNode.next;
    }
    previousNode.next = newNode;
    newNode.next = currentNode;

    return head;
}

/*linkedlist = insert(node1, 4)
traverse(node);*/

function remove(head, node) {
    if (head == node) {
        head = node.next;
        node.next = null;
    } else {
        while (head.next != null ) {
            if (head.next == node ) {
                head.next = node.next;
                node.next = null;
                break;
            }
            head = head.next;
        }
    }
    return head;
}

linkedlist = remove(node1, node2);
traverse(node1);
