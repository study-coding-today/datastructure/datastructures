function swap(elements, index1, index2) {
    let temp = elements[index1];
    elements[index1] = elements[index2];
    elements[index2] = temp;
}

function heapify(elements, n, i) {
    let largest = i;
    let left = 2 * i + 1;
    let right = 2 * i + 2;

    if (left < n && elements[left] > elements[largest]) {
        largest = left;
    };

    if (right < n && elements[right] > elements[largest]) {
        largest = right;
    }

    if (largest != i) {
        swap(elements, i, largest);
        heapify(elements, n, largest);
    };
};

function heap_sort(elements) {
    let n = elements.length;

    for (let i = Math.floor(n / 2); i >= 0; i--) {
        heapify(elements, n, i);
    }

    for (let i = n - 1; i > 0; i--) {
        swap(elements, 0, i);
        n--;
        heapify(elements, i, 0);
    };
    return elements;
};

console.log(heap_sort([4, 1, 9, 0, 2]))