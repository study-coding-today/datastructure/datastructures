function insertionSort(elements) {
    for (let i = 1; i < elements.length; i++) {
        let value = elements[i];
        let j = i - 1;
        while (j >= 0 && value < elements[j]) {
            elements[j + 1] = elements[j];
            j -= 1;
        }
        elements[j + 1] = value;
    }
    return elements;
}

console.log(insertionSort([5, 0, 2, 1, 10, 9, 8]));