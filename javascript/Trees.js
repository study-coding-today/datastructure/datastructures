class Node {
    constructor(data, left, right) {
        this.data =data;
        this.left = left;
        this.right = right;
    };
};

var node1 = new Node("c", null, null);
var node2 = new Node("b", null, null);
var node3 = new Node("a", null, null);

function getHeight(root) {
    if (root == null){
        return 0;
    };
    return Math.max(getHeight(root.left), getHeight(root.right) + 1);
};

function isBalanced(root) {
    if (root == null) {
        return true;
    };

    var diff = 0;

    if ((root.left != null && root.right == null) || (root.left == null && root.right != null) ) {
        diff += 1;
    } else {
        diff = 0;
    };
    return isBalanced(root.left) && isBalanced(root.right);
};

function preOrder(root) {
    if (root) {
        console.log(root.data);
        preOrder(root.left);
        preOrder(root.right);
    };
};

function inOrder(root) {
    if (root) {
        inOrder(root.left);
        console.log(root.data);
        inOrder(root.right);
    };
};

function postOrder(root) {
    if (root) {
        postOrder(root.left);
        postOrder(root.right);
        console.log(root.data);
    };
};

var nodeG = new Node("g", null, null);
var nodeF = new Node("f", null, null);
var nodeE = new Node("e", null, null);
var nodeD = new Node("d", null, null);
var nodeC = new Node("c", nodeF, nodeG);
var nodeB = new Node("b", nodeD, nodeE);
var nodeA = new Node("a", nodeB, nodeA);

function breadthFirstSearch(root) {
    var nodes = [];
    if (root == null) {
        return;
    };
    nodes.unshift(root);
    while (nodes.length > 0) {
        var node = nodes.pop();
        if (node != null) {
            console.log(node.data);
            if (node.left != null) {
                nodes.unshift(node.left);
            }
            if (node.right != null) {
                nodes.unshift(node.right);
            };
        };
    };
};

function depthFirstSearch(root) {
    var nodes = [];
    if (root == null) {
        return;
    };
    nodes.push(root);
    while (nodes.length > 0) {
        var node = nodes.pop();
        if (node.right != null) {
            nodes.push(node.right);
        };
        if (node.left != null) {
            nodes.push(node.left);
        };
        console.log(node.data);
    };
};

depthFirstSearch(nodeA);