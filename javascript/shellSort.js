function shell_sort(elements) {
    let length = elements.length;
    let gap = Math.floor(length / 2);
    while (gap > 0) {
        for (let i = gap; i < length; i++) {
            let temp = elements[i];
            let j = i;
            while (j >= gap && elements[j - gap] > temp) {
                elements[j] = elements[j - gap];
                j -= gap;
            };
            elements[j] = temp;
        };
        if (gap == 2) {
            gap = 1;
        } else {
            gap = parseInt(gap * 5 / 11);
        };
    };
    return elements;
};


console.log(shell_sort([9, 1, 0, 2, 5]));