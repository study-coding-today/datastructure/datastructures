function interpolationSearch(elements, x) {
    let low = 0;
    let high = elements.length - 1;
    while (low <= high && x >= elements[low] && x <= elements[high]) {
        if (low === high) {
            if (elements[low] == x) {
                return low;
            } else {
                return -1;
            }
        }
        let pos = (low + (high - low) / (elements[high] - elements[low] * (x - elements[low])));

        if (elements[pos] == x) {
            return pos;
        }

        if (elements[pos] < x) {
            low = pos + 1;
        } else {
            high = pos -1;
        }
    }
    return -1;
}

console.log(interpolationSearch([1,2,3,4], 5));