function selectionSort(elements) {
    let length = elements.length;
    for (let i = 0; i < length; i++) {
        let minIndex = i;
        for (let j = i + 1; j < length; j++) {
            if (elements[j] < elements[minIndex]) {
                minIndex = j;
            }
            let temp = elements[i];
            elements[i] = elements[minIndex];
            elements[minIndex] = temp;
        }
        return elements;
    }
};

console.log(selectionSort([2, 1, 5, 0]))