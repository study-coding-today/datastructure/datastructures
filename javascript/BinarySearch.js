// 데이터 set
let arr = [];
console.log("데이터를 만들고 있음");
for (let i = 0; i < 3000000; i++) {
    arr.push( parseInt(Math.random() * 200000) );
};
console.log("데이터를 완성");


// 퀵정렬
console.log("퀵정렬 시작");
let startSortTime = new Date();
arr.sort( (a, b) => {
    return a - b;
});
console.log("퀵정렬 완료");
let endSortTime = new Date();
console.log(((endSortTime.getTime() - startSortTime.getTime())/1000) +"초가 경과됨")


// 바이너리 탐색법
console.log("바이너리탐색 시작")
let startSearchTime = new Date();
let index = binaryRecursiveSearch(arr, 0, arr.length - 1, 70817);
if (index === -1) {
    console.log("배열에서 70817을 찾지 못했습니다.")
} else {
    console.log("배열의"+ index +"번 인덱스에서 70817을 찾았습니다.")
}
console.log("바이너리탐색 완료");
let endSearchTime = new Date();
console.log(((endSearchTime.getTime() - startSearchTime.getTime())/1000) +"초가 경과됨");


// 이진탐색 재귀호출
function binaryRecursiveSearch(arr, startIdx, endIdx, value) {
    let targetIdx = startIdx + parseInt((endIdx - startIdx)/2);
    if (startIdx === endIdx && arr[targetIdx] !== value) {
        // 찾지 못했을 때
        return -1;
    };
    if (arr[targetIdx] > value) {
        // 중간값이 찾고자 하는 값보다 크면 찾는 데이터는 중간값보다 왼쪽
        // 새로 설정된 탐색범위는 startIdx ~ targetIdx 가 된다.
        return binaryRecursiveSearch(arr, startIdx, targetIdx, value);

    };
    if (arr[targetIdx] < value) {
        // 중간값이 찾고자 하는 값보다 작으면 찾는 데이터는 중간값보다 오른쪽
        // 새로 설정된 탐색범위는 targetIdx ~ endIdx 가 된다.
        return binaryRecursiveSearch(arr, targetIdx, endIdx, value);
    };
    if (arr[targetIdx] === value) {
        // 데이터 찾음
        return targetIdx;
    };
};

// 이진탐색 비재귀호출
function binarySearch(arr, startIdx, endIdx, value) {
    // targetIdx 가 value 와 비교하면서 startIdx 와 endIdx 가 targetIdx 가 된다.
    for ( let i = startIdx; i < endIdx - startIdx; i++ ) {
        let targetIdx = startIdx + parseInt((endIdx - startIdx)/2);
        if (startIdx === endIdx && arr[targetIdx] !== value) {
            // 찾지 못했을 때
            return -1;
        }
        if (arr[targetIdx] > value) {
            // 중간값이 찾고자 하는 값보다 크면 찾는 데이터는 중간값보다 왼쪽
            // 새로 설정된 탐색범위는 endIdx 가 targetIdx 가 된다.
            endIdx = targetIdx;

        }
        if (arr[targetIdx] < value) {
            // 중간값이 찾고자 하는 값보다 작으면 찾는 데이터는 중간값보다 오른쪽
            // 새로 설정된 탐색범위는 startIdx 가 targetIdx 가 된다.
            startIdx = targetIdx;
        }
        if (arr[targetIdx] === value) {
            // 데이터 찾음
            return targetIdx;
        }
    }
};