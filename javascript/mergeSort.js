function mere(left, right) {
    let elements = [];
    let left_index = 0;
    let right_index = 0;

    while (left_index < left.length && right_index < right.length) {
        if (left[left_index] < right[right_index]) {
            elements.push(left[left_index]);
            left_index++;
        } else {
            elements.push(right[right_index]);
            right_index++;
        }
    }
    return elements.concat(left.slice(left_index)).concat(right.slice(right_index));
}

function merge_sort(elements) {
    if (elements.length <= 1) {
        return elements;
    }

    let middle = Math.floor(elements.length / 2);
    let left = elements.slice(0, middle);
    let right = elements.slice(middle);

    return mere(merge_sort(left), merge_sort(right));
}

let elements = [3, 0, 5, 1, 2];
console.log(merge_sort(elements, 0, elements.length - 1));
