function bubble_sort(elements) {
    var length = elements.length;
    for (var i = 0; i < length; i++) {
        for (var j = 0; j < length; j++) {
            if (elements[j] > elements[j + 1]) {
                var temp = elements[j];
                elements[j] = elements[j + 1];
                elements[j+1] = temp;
            }
        }
    }
    return elements;
}

console.log(bubble_sort([2, 0, 3, 1]));