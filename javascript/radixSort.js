function radixSort(elements) {
    let max = Math.max(...elements) * 10;
    let divisor = 10;
    while (divisor < max) {
        let buckets = [...Array(10)].map(() => []);
        for (let num of elements) {
            buckets[Math.floor((num % divisor) / (divisor / 10))].push(num);
        };
        elements = [].concat.apply([], buckets);
        divisor *= 10;
    };
    return elements;
};

console.log(radixSort([401, 503, 104]));