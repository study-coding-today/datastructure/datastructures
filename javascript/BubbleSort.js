// 버블정렬 #1
let arr = [3, 6, 8, 2, 1, 9, 7, 4];

while (true) {
    let count = 0;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > arr[i + 1]) {
            let temp = arr[i];
            arr[i] = arr[i+1];
            arr[i+1] = temp;
            count += 1;
        }
    }

    if (count === 0) {
        break;
    }
}
console.log(arr);

// 버블정렬 #2-1
let arrA= [3, 6, 8, 2, 1, 9, 7, 4];
function bubble_sort(arrA) {
    // n개의 수 비교시 n-1개를 비교해야 한다. 마지막 숫자는 앞 숫자에 의해 다 비교됨
    for (let i = arrA.length-1; i > 0 ; i--) {
        for ( let j = 0; j < i; j++ ) {
            if ( arrA[j] > arrA[j+1] ) {
                let temp = arrA[j];
                arrA[j]  = arrA[j+1];
                arrA[j+1] = temp;
            }
        }
    }
    console.log(arrA);
}
bubble_sort(arrA);

// 버블정렬 #2-2
let arrB = [3, 6, 8, 2, 1, 9, 7, 4];
function bubble_sort(arrB) {
    // n개의 수 비교시 n-1개를 비교해야 한다. 마지막 숫자는 앞 숫자에 의해 다 비교됨
    for (let i = 0; i < arrB.length-1 ; i++) {
        for ( let j = 0; j < (arrB.length-1)-i; j++ ) {
            if ( arrB[j] > arrB[j+1] ) {
                let temp = arrB[j];
                arrB[j]  = arrB[j+1];
                arrB[j+1] = temp;
            }
        }
    }
    return arrB;
}
let arrSorted = bubble_sort(arrB);
console.log(arrSorted);