function linear_search(elements, x) {
    let length = elements.length;
    for (let i = 0; i < length; i++) {
        if (elements[i] == x) {
            return i;
        }
    }
    return -1
}

console.log(linear_search([1, 2, 3, 4], 2))