function swap(elements, index1, index2) {
    let temp = elements[index1];
    elements[index1] = elements[index2];
    elements[index2] = temp;
};

function partition(elements, left, right) {
    let pivot = elements[Math.floor((right + left) / 2)];
    let i = left;
    let j = right;

    while (i < j) {
        while (elements[i] < pivot) {
            i++;
        };

        while ((elements[j] < pivot)) {
            j--;
        };
        if (i <= j) {
            swap(elements, i, j);
            i++;
            j--;
        };
    };
    return i;
};

function quick_sort(elements, left, right) {
    let index;

    if (elements.length > 1) {
        index = partition(elements, left, right);

        if (left < index -1) {
            quick_sort(elements, index, right)
        };

        if (index < right) {
            quick_sort(elements, index, right);
        };
    };
    return elements;
};

let elements = [9, 1, 2, 0, 10, 3]
quick_sort(elements, 0, elements.length - 1);
console.log(elements);