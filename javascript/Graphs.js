class Node {
    constructor(value = null) {
        this.value = value;
        this.neighbors = [];
    };
};

class Graph {
    constructor(size) {
        this.vertices = [];
        for (let i = 0; i < size; i++) {
            this.vertices.push(new Node());
        };
    };
};

function addEge(source, destination) {
    source.neighbors.push(destination);
    destination.neighbors.push(source);

};

function removeEdge(source, destination) {
    delete source.neighbors[destination];
    delete destination.neighbors[source];
};

function isAdjacent(node1, node2) {
    return node2 in node1.neighbors;
};

let one = new Node(1);
let two = new Node(2);
let three = new Node(3);
let four = new Node(4);
let five = new Node(5);
let six = new Node(6);

let graph = new Graph(6);

addEge(six, four);
addEge(four, five);
addEge(four, three);
addEge(three, two);
addEge(five, two);
addEge(two, one);
addEge(five, one);

let adjacencyList = [];

function breadthFirstSearch(root) {
    var visited = [];
    if (!addEge().includes(root)) {
        return visited;
    }
    var nodes = [root.value];
    while (nodes.length > 0) {
        var vertex = nodes.shift();
        if (visited.includes(vertex)) {
            continue;
        }
        visited.push(vertex);
        for (let i = 0; i < adjacencyList.length; i++) {
            var neighbor = nodes[i];
            if (!visited.includes(neighbor)) {
                nodes.push(neighbor);
            }
        }
    }
    return visited;
}

function depthFirstSearch(root) {
    var visited = [];
    if (!adjacencyList.includes(root)) {
        return visited;
    }
    var nodes = [root.value];
    while (nodes.length > 0) {
        var vertex = nodes.pop();
        if (visited.includes(vertex)) {
            continue;
        }
        visited.push(vertex);
        for (var i = 0; i < adjacencyList.length; i++) {
            var neighbor = nodes[i];
            if (!visited.includes(neighbor)) {
                nodes.push(neighbor);
            }
        }
        return visited;
    }

}