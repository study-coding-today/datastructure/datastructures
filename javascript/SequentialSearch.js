// 데이터 set
let arr = [];
console.log("데이터를 만들고 있음");
for (let i = 0; i < 50000; i++) {
    arr.push( parseInt(Math.random() * 200000) );
};
console.log("데이터를 완성");

console.log("버블정렬 시작")
let startSortTime = new Date();
while (true) {
    let count = 0;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > arr[i + 1]) {
            let temp = arr[i];
            arr[i] = arr[i+1];
            arr[i+1] = temp;
            count += 1;
        };
    };

    if (count === 0) {
        break;
    };
};
console.log("버블정렬 완료");
let endSortTime = new Date();
console.log(((endSortTime.getTime() - startSortTime.getTime())/1000) +"초가 경과됨");

// 순자탐색법
let startSearchTime = new Date();
let index = -1;
for (let i = 0; i < arr.length; i++) {
    if (arr[i] === 70817) {
        index= i;
        break;
    };
};
if (index === -1) {
    console.log("배열에서 70817을 찾지 못했습니다.")
} else {
    console.log("배열의"+ index +"번 인덱스에서 7을 찾았습니다.")
};
let endSearchTime = new Date();
console.log(((endSearchTime.getTime() - startSearchTime.getTime())/1000) +"초가 경과됨");