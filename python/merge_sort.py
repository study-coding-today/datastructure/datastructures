def merge(elements, left, middle, right):
    n1 = middle - left + 1
    n2 = right - middle

    left_sublist = [0] * (n1)
    right_sublist = [0] * (n2)

    for i in range(0, n1):
        left_sublist[i] = elements[left + i]

    for j in range(0, n2):
        right_sublist[j] = elements[middle + 1 + j]

    i = 0
    j = 0
    k = left

    while i < n1 and j < n2:
        if left_sublist[i] <= right_sublist[j]:
            elements[k] = left_sublist[i]
            i += 1
        else:
            elements[k] = right_sublist[j]
            j += 1
        k += 1

    while i < n1:
        elements[k] = left_sublist[i]
        i += 1
        k += 1

    while j < n2:
        elements[k] = right_sublist[j]
        j += 1
        k += 1

    return elements


def merge_sort(elements, left, right):
    if left < right:
        middle = (left + (right - 1)) // 2
        merge_sort(elements, left, middle)
        merge_sort(elements, middle+1, right)
        merge(elements, left, middle, right)

    return elements


elements = [3, 0, 5, 1, 2]
print(merge_sort(elements, 0, len(elements)-1))
