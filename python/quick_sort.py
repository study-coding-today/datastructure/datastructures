def partition(elements, left, right):
    pivot = elements[left]
    low = left + 1
    high = right

    while True:
        while low <= high and elements[high] >= pivot:
            high -= 1
        while low <= high and elements[low] <= pivot:
            low += 1
        if low <= high:
            elements[low], elements[high] = elements[high], elements[low]
        else:
            break

    elements[left], elements[high] = elements[high], elements[left]
    return high


def quick_sort(elements, left, right):
    if left >= right:
        return

    p = partition(elements, left, right)
    quick_sort(elements, left, p-1)
    quick_sort(elements, p+1, p-1)


elements = [9, 1, 2, 0, 10, 3]
quick_sort(elements, 0, len(elements) - 1)
print(elements)