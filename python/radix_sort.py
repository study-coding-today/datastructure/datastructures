def radix_sort(elements):
    placement = 1
    max_digit = max(elements)

    while placement < max_digit:
        buckets = [list() for _ in range(10)]
        for i in elements:
            temp = int((i / placement) % 10)
            buckets[temp].append(i)
        a = 0
        for b in range(10):
            buck = buckets[b]
            for i in buck:
                elements[a] = i
                a += 1
        placement *= 10
    return elements


print(radix_sort([401, 503, 104]))
