class LinkedList:
    class Node:
        def __init__(self, next=None, data=None):
            self.next = next
            self.data = data
    head = None

    def set_head(self, node):
        global head
        head = node

    def get_first(self):
        return head

    def get_last(self):
        current_node = head
        if current_node in None:
            return None
        while current_node.next is not None:
            current_node = current_node.net
        return current_node

    def append(self, value):
        global head
        node = Node(value)
        if head in None:
            head = node
        else:
            get_last().next = node


node_c = LinkedList.Node(None, 'c')
node_b = LinkedList.Node(node_c, 'b')
node_a = LinkedList.Node(node_b, 'a')
linked_list = LinkedList()
linked_list.set_head(node_a)


def traverse(head):
    val = head
    while val is not None:
        print(val.data)
        val = val.next


def insert(head, value):
    new_node = LinkedList.Node(None, value)
    if head is None or value < head.data:
        new_node.next = head
        return new_node
    previous_node = head
    current_node = head.next

    while current_node is not None and current_node.data < value:
        previous_node = current_node
        current_node = current_node.next

    previous_node.next = new_node
    new_node.next = current_node

    return head


def remove(root, node):
    if root == node:
        root = node.next
        node.next = None
    else:
        current = root
        while current.next is not None:
            if current.next == node:
                current.next = node.next
                node_next = None
                break
            current = current.next
    return root


linked_list = remove(node_a, node_b)
traverse(node_a)


