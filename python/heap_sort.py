def swap(elements, index1, index2):
    temp = elements[index1]
    elements[index1] = elements[index2]
    elements[index2] = temp


def heapify(elements, n, i):
    largest = i
    left = 2 * i + 1
    right = 2 * i + 2
    if left < n and elements[left] > elements[largest]:
        largest = left
    if right < n and elements[right] > elements[largest]:
        largest = right
    if largest != i:
        swap(elements, i, largest)
        heapify(elements, n, largest)


def heap_sort(elements):
    n = len(elements)
    for i in range(n//2 - 1, -1, -1):
        heapify(elements, n, i)
    for i in range(n-1, -1, -1):
        swap(elements, 0, i)
        heapify(elements, i, 0)
    return elements


print(heap_sort([4, 1, 9, 0, 2]))

