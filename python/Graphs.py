class Node:
    neighbors = []
    def __init__(self, value=None):
        self.value = value


class Graph:
    def __init__(self, size):
        vertices = []
        for i in range(size):
            vertices.append(Node())

    def add_edge(self, source, destination):
        source.neighbors.append(destination)
        destination.neighbors.append(source)

    def remove_edge(self, source, destination):
        source.neighbors.pop(destination)
        destination.neighbors.pop(source)

    def is_adjacent(self, node1, node2):
        return node2 in node1.neighbors


one = Node(1)
two = Node(2)
three = Node(3)
four = Node(4)
five = Node(5)
six = Node(6)

graph =Graph(6)

graph.add_edge(six, four)
graph.add_edge(four, five)
graph.add_edge(four, three)
graph.add_edge(three, two)
graph.add_edge(five, two)
graph.add_edge(two, one)
graph.add_edge(five, one)

adjacency_list = []


def depth_first_search(root):
    visited = []
    if root not in adjacency_list:
        return visited
    nodes = [root.value]
    while len(nodes) > 0:
        vertex = nodes.pop()
        if vertex in visited:
            continue
        visited.append(vertex)
        for i in range(len(adjacency_list)):
            neighbor = nodes[i]
            if neighbor not in visited:
                nodes.append(neighbor)

    return visited

