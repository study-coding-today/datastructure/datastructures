def selection_sort(elements):
    length = len(elements)
    for i in range(length):
        min_index = i
        for j in range(i+1, length):
            if elements[j] < elements[min_index]:
                min_index = j
            temp = elements[i]
            elements[i] = elements[min_index]
            elements[min_index] = temp
        return elements


print(selection_sort([2, 1, 5, 0]))
