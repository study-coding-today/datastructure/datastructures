def interpolation_search(elements, x):
    low = 0
    high = len(elements) - 1

    while low <= high and x >= elements[low] and x <= elements[high]:
        if low == high:
            if elements[low] == x:
                return low
            else:
                return -1

        pos = int(low + (high - low) / (elements[high] - elements[low] * (x - elements[low])))

        if elements[pos] == x:
            return pos

        if elements[pos] < x:
            low = pos + 1
        else:
            high = pos - 1
    return -1


print(interpolation_search([1, 2, 3, 4], 5))