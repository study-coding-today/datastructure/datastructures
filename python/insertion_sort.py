def insertion_sort(elements):
    for i in range(1, len(elements)):
        value = elements[i]
        j = i - 1
        while j >= 0 and value < elements[j]:
            elements[j + 1] = elements[j]
            j -= 1
            elements[j + 1] = value
    return elements


print(insertion_sort([5, 2, 0, 1, 10, 9, 8]))
