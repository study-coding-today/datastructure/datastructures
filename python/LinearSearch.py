def linear_search(elements, x):
    length = len(elements)
    for i in range(length):
        if elements[i] == x:
            return i

    return -1


print(linear_search([1, 2, 3, 4], 5))
