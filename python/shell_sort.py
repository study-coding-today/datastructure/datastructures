def shell_sort(elements):
    length = len(elements)
    gap = length // 2

    while gap > 0:
        for i in range(gap, length):
            temp = elements[i]
            j = i
            while j >= gap and elements[j - gap] > temp:
                elements[j] = elements[j - gap]
                j -= gap
            elements[j] = temp
        gap //= 2

    return elements


print(shell_sort([9, 1, 0, 2, 5]))