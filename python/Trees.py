class Node:
    def __init__(self, data, left, right):
        self.data = data
        self.left = left
        self.right = right


node1 = Node("c", None, None)
node2 = Node("b", None, None)
node3 = Node("a", node2, node1)


def get_height(root):
    if root == None:
        return 0
    return max(get_height(root.left), get_height(root.right) + 1 )


def is_balanced(root):
    if root is None:
        return True
    diff = 0
    if (root.left is not None and root.right is None) or (root.left is None and root.right is not None):
        diff += 1
    else:
        diff = 0
    return is_balanced(root.left) and is_balanced(root.right)


def pre_order(root):
    if root:
        print(root.data)
        pre_order(root.left)
        pre_order(root.right)


def in_order(root):
    if root:
        in_order(root.left)
        print(root.data)
        in_order(root.right)


def post_order(root):
    if root:
        post_order(root.left)
        post_order(root.right)
        print(root.data)


node_g = Node("g", None, None)
node_f = Node("f", None, None)
node_e = Node("e", None, None)
node_d = Node("d", None, None)
node_c = Node("c", node_f, node_g)
node_b = Node("b", node_d, node_e)
node_a = Node("a", node_b, node_c)


def breadth_first_search(root):
    nodes = []
    if root in None:
        return
    nodes.insert(0, root)
    while len(nodes) > 0:
        node = nodes.pop()
        if node:
            print(node.data)
            if node.left is not None:
                nodes.insert(0, node.left)
            if node.right is not None:
                nodes.insert(0, node.right)


def depth_first_search(root):
    if root is None:
        return

    print(root.data)
    depth_first_search(root.left)
    depth_first_search(root.right)

    
depth_first_search(node_a)
